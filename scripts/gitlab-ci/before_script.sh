#!/bin/bash
#
# This build script is licensed under CC0 1.0 Universal:
# https://creativecommons.org/publicdomain/zero/1.0/

set -euo pipefail

export DIR
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "$DIR/_common.sh"

# Install dependencies and prepare stuff before building
apt-get update -yqq
apt-get install -y \
    git cmake wget curl zip unzip build-essential ninja-build pkg-config \
    libwxgtk3.0-dev libwxgtk-media3.0-dev libwxgtk-webview3.0-dev \
    libgtk2.0-dev libfluidsynth-dev libfreeimage-dev \
    libsfml-dev libftgl-dev libglew-dev libcurl4-gnutls-dev libbz2-dev

git clone --depth=1 "https://github.com/sirjuddington/SLADE.git" slade
mkdir -p "$ARTIFACTS_DIR"

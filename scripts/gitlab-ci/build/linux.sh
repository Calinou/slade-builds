#!/bin/bash
#
# This build script is licensed under CC0 1.0 Universal:
# https://creativecommons.org/publicdomain/zero/1.0/

set -euo pipefail

export DIR
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "$DIR/../_common.sh"

# Build SLADE for Linux
cd "build/"
cmake \
    .. \
    -DCMAKE_BUILD_TYPE="Release" \
    -DCMAKE_INSTALL_PREFIX="$(pwd)/appdir/usr" \
    -G "Ninja"
cmake --build . -- install

strip "appdir/usr/bin/slade"
mv "$CI_PROJECT_DIR/resources/slade.desktop" "appdir/slade.desktop"
mv "appdir/usr/share/icons/net.mancubus.SLADE.png" "appdir/slade.png"
# Remove unnecessary files
rm -rf "appdir/usr/share/appdata" "appdir/usr/share/applications" "appdir/usr/share/icons"
curl -LOSs "https://github.com/probonopd/linuxdeployqt/releases/download/continuous/linuxdeployqt-continuous-x86_64.AppImage"
chmod +x "linuxdeployqt-continuous-x86_64.AppImage"
./linuxdeployqt-continuous-x86_64.AppImage --appimage-extract
./squashfs-root/AppRun "appdir/slade.desktop" -appimage

mv "SLADE-x86_64.AppImage" "$ARTIFACTS_DIR/slade-nightly-x86_64.AppImage"
